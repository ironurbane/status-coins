.. Status documentation master file, created by
   sphinx-quickstart on Wed Jun 14 22:26:46 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


===============================================
Status, 我支持你!  <3  Status, I will be there for u always & forever!
===============================================


**标签:** Status | SGT | SNT | Jarrad | Carl


----------------------
1.介绍(Introduce):
----------------------

    Status 连接你我他。现在已发布内测版本(Alpha，可以找我获取) 。官网请移步：https://status.im


    .. image:: ./_static/cc.png

    .. image:: ./_static/bb.png


----------------------
2.我在行动(I am  in action):
----------------------

#a
    ---我在著名程序员博客ITEYE发表Status推文信息. I post status info via famous it blog (ITEYE) in China

    --- http://ironurbane.iteye.com/blog/2379456

    --- 截图(ScreenShot)：

    .. image:: ./_static/tips.png



#b
    ---我在著名程序员博客开源中囯社区博客发表Status推文信息. I post status info via famous it blog（OSCHINA）in China

    ---https://my.oschina.net/u/2395742/blog/956665

    --- 截图(ScreenShot)：

    .. image:: ./_static/tips1.png



#c
    ---我在著名程序员博客CHINAUNIX发表Status推文信息.  I post status info via famous it blogI (CHINAUNIX) in China

    ---http://blog.chinaunix.net/uid-15754892-id-5766267.html 

    --- 截图(ScreenShot)：

    .. image:: ./_static/tips2.png



#d
    ---我在著名程序员博客ITPUB发表Status推文信息. I post status info via famous it blog (ITPUB) in China

    ---http://www.itpub.net/thread-2088804-1-1.html 

    --- 截图(ScreenShot)：

    .. image:: ./_static/tips3.png



#e
    ---我在中囯最著名社交网站QQ ZONE发表Status推文信息. I post status info via famous society media network (QQ ZONE) in China

    ---https://user.qzone.qq.com/61366756 

    --- 截图(ScreenShot)：

    .. image:: ./_static/tips4.png


